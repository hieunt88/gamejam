'use strict';

// Global vars
var game = game || {},
	canvas = document.getElementById("canvas"),
	canvasEnv = document.getElementById("canvasEnv"),
	canvasWidth = canvas.width = canvasEnv.width = document.body.clientWidth,
	canvasHeight = canvas.height = canvasEnv.height = document.body.clientHeight,
	canvasWidthHalf = canvasWidth * 0.5,
	canvasHeightHalf = canvasHeight * 0.5,
	ctx = canvas.getContext("2d"),
	ctxEnv = canvasEnv.getContext("2d"),
	fps = 60,
	gameover = false,
	debug = false,
	debugLogs = [];

	ctx.imageSmoothingEnabled = true;	
	ctxEnv.imageSmoothingEnabled = false;

var planet = document.getElementById("planet"),
	planetRadius = planet.offsetWidth * 0.5;

	console.log(planetRadius);

var playerMaxVelocity = 2,
	playerSpeedDefault = 0.2;
	
var goalCount = 1;

var colors = {
	lighter: '252, 252, 250',
	light: '231, 227, 202',
	dark: '43, 42, 60',
	darker: '24, 18, 32'
};


var gameContainer = document.getElementById('game'), 
	gameDisplayTime = null,
	gameDisplayScore = 0,
	// interfaceUpdateFrequency = 1,
	// interfaceTimer = 0,
	timeBox = document.getElementById('time'),
	scoreBox = document.getElementById('score');



// var logoImage = new Image();
// logoImage.src = 'public/assets/img/ggj-logo.svg';

/*
 *  -- Component Imports --
 */

// import('components/_keymappings.js');
// import('components/_update.js');
// import('components/_render.js');
// import('components/_event-loop.js');
// import('components/_entities.js');
// import('components/_environment.js');

/*
 *  -- Component Functions --
 */

document.addEventListener("DOMContentLoaded", function() {
	game.keymappings.init();
	game.eventLoop.init();
});
