/* 
 *	_event-loop.js
 */

game.eventLoop = (function () {
	'use strict';

	var paused = false,
		pauseStep = false,
		dt = 0,
		dtInit = Date.now(); //new Date();

	var togglePaused = function (e) {
		paused = !paused;

		if (paused) {
		 	gameContainer.classList.add('game-paused');
		} else {
			gameContainer.classList.remove('game-paused');
		}
	};

	var calculateDeltaTime = function () {
		var dtNow = new Date();
		dt = dtNow - dtInit;
		// fps = 1 / dt;
	};

	var loop = function () {
		setTimeout(function () {

			if (game.keymappings.keys.ControlLeft === true) {
				pauseStep = true;
			}

			if (gameover === false) {
				if (paused === false || pauseStep === true) {
					pauseStep = false;

					game.update.canvas();
					game.update.env();
					game.update.players();
					game.update.goals();
					game.update.gameOverCheck();

					game.render.canvas();
					game.render.env();
					// game.render.planet();
					game.render.players();
					game.render.goals();
					game.render.UI();

					if (debug) {
						game.render.debugWindow();
					}

					// var gamePads = navigator.getGamepads();
					// console.log(gamePads[1]);
				}
			}

			window.requestAnimationFrame(loop);
			
		}, 1000 / fps);
	};

	var init = function () {		
				
		document.addEventListener('click', togglePaused, false);
		ctx.translate(canvasWidth * 0.5, canvasHeight * 0.5);
		ctxEnv.translate(canvasWidth * 0.5, canvasHeight * 0.5);

		game.env = game.Environment.create();
		timeBox.innerHTML = gameDisplayTime;

		// Create players
		game.entities.Player.create(
			0,
			-planetRadius,
			'rgb(' + colors.lighter + ')',
			'rgb(' + colors.darker + ')',
			playerSpeedDefault,
			Math.PI * 0.5,
			1,
			playerMaxVelocity
		);

		game.entities.Player.create(
			0,
			-planetRadius,
			'rgb(' + colors.darker + ')',
			'rgb(' + colors.lighter + ')',
			playerSpeedDefault,
			Math.PI * 1.5,
			-1, 
			playerMaxVelocity
		);

		game.entities.Goal.create(0, 0, Math.random() * (Math.PI * 2), 0);

		// if (paused === false) {
			loop();
		// }
	};

	return {
		dt: dt,
		init: init
	};

}());