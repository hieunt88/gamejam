/* 
 *	_render.js
 */

game.render = (function () {
	'use strict';

	var canvas = function () {
		ctx.clearRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);
		ctxEnv.clearRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);

		ctx.fillStyle = 'rgb(' + colors.dark + ')';
		ctx.strokeStyle = 'rgb(' + colors.lighter + ')';

		if (debug === true) {
			// ctx.fillStyle = 'rgb(255,255,255)';
		}

		ctx.fillRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);
	};

	// var imgTextureLight = new Image();
	// imgTextureLight.src = 'public/assets/img/tex-planet-light-1.jpg';

	var planet = function () {
		ctx.save();
		ctx.rotate(game.env.time + (Math.PI * 0.5));			
		// ctx.strokeStyle = "rgb(0,0,0)";
		ctx.beginPath();
		ctx.arc(0, 0, planetRadius, 0, Math.PI * 2, false);
		ctx.strokeStyle = colors.darker;
		ctx.stroke();
		// ctx.clip();

		// ctx.drawImage(logoImage, -500, -1000 - game.env.bias);
		
		ctx.restore();
		
	};

	var goals = function () {
		var goal = game.entities.Goal.current();

		ctx.save();

		ctx.fillStyle = goal.color1;
		ctx.strokeStyle = goal.color2;
		ctx.rotate(goal.angle);
		ctx.beginPath();
		ctx.arc(0, goal.position._y - (goal.height * 0.5), goal.width * 0.5, 0, Math.PI * 2, false);
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		if (debug) {
			// Collision box
			var halfW = goal.width * 0.5,
				halfH = goal.height * 0.5;

			ctx.strokeStyle = ctx.fillStyle = 'rgb(165,0,0)';
			ctx.beginPath();
			ctx.moveTo(goal.collisionBox.p1.x, goal.collisionBox.p1.y);
			ctx.lineTo(goal.collisionBox.p2.x, goal.collisionBox.p1.y);
			ctx.lineTo(goal.collisionBox.p2.x, goal.collisionBox.p2.y);
			ctx.lineTo(goal.collisionBox.p1.x, goal.collisionBox.p2.y);
			ctx.lineTo(goal.collisionBox.p1.x, goal.collisionBox.p1.y);
			ctx.stroke();

			if (goal.collision) {
				ctx.fill();
			}

			// Goal coords
			ctx.fillStyle = 'rgb(0,0,0)';
			ctx.fillText('ID: ' + goal.ID, goal.coord._x + 1, goal.coord._y + 1);
			ctx.fillText('(' + goal.coord._x.toFixed(2) + ',' + goal.coord._y.toFixed(2) + ')', goal.coord._x + 1, goal.coord._y + 21);
			ctx.fillStyle = 'rgb(255,255,255)';
			ctx.fillText('ID: ' + goal.ID, goal.coord._x, goal.coord._y);
			ctx.fillText('(' + goal.coord._x.toFixed(2) + ',' + goal.coord._y.toFixed(2) + ')', goal.coord._x, goal.coord._y + 20);
		}
	};

	var players = function () {
		for (var i = 0; i < game.entities.players.length; i++) {
			var player = game.entities.players[i];

			// if (player.lit === true) {
			// 	ctx.fillStyle = 'rgb(' + colors.light + ')';
			// 	ctx.strokeStyle = 'rgb(' + colors.dark + ')';
			// } else {
			// 	ctx.fillStyle = 'rgb(' + colors.dark + ')';
			// 	ctx.strokeStyle = 'rgb(' + colors.lighter + ')';
			// }

			ctx.fillStyle = player.color1;
			ctx.strokeStyle = player.color2;

			ctx.save();
			ctx.rotate(player.angle);

			ctx.lineWidth = 2;
			ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
			ctx.beginPath();
			ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.5, 0, Math.PI * 2, false);
			ctx.stroke();
			ctx.fill();
			
			if (player.ID === 0 && player.lit === true) {
				ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
				ctx.beginPath();
				ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.2, 0, Math.PI * 2, false);
				ctx.fillStyle = player.color2;
				ctx.fill();
			}

			if (player.ID === 1 && player.lit === false) {
				ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
				ctx.beginPath();
				ctx.fillStyle = player.color2;
				ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.2, 0, Math.PI * 2, false);
				ctx.fill();
			}

			// ctx.clip();
			// ctx.drawImage(player.texture, player.position._x - (player.width * 0.5), player.position._y - player.height);
			// ctx.stroke();

			// ctx.fillRect(player.position._x - (player.width * 0.5), player.position._y - player.height, player.width, player.height);
			ctx.restore();

			if (debug) {
				// Collision box
				var halfW = player.width * 0.5,
					halfH = player.height * 0.5;

				ctx.strokeStyle = ctx.fillStyle = 'rgb(165,0,0)';
				ctx.beginPath();
				ctx.moveTo(player.collisionBox.p1.x, player.collisionBox.p1.y);
				ctx.lineTo(player.collisionBox.p2.x, player.collisionBox.p1.y);
				ctx.lineTo(player.collisionBox.p2.x, player.collisionBox.p2.y);
				ctx.lineTo(player.collisionBox.p1.x, player.collisionBox.p2.y);
				ctx.lineTo(player.collisionBox.p1.x, player.collisionBox.p1.y);
				ctx.stroke();

				if (player.collision) {
					ctx.fill();
				}

				// Player coords
				ctx.fillStyle = 'rgb(0,0,0)';
				ctx.fillText('ID: ' + player.ID, player.coord._x + 1, player.coord._y + 1);
				ctx.fillText('(' + player.coord._x.toFixed(2) + ',' + player.coord._y.toFixed(2) + ')', player.coord._x + 1, player.coord._y + 21);
				ctx.fillStyle = 'rgb(255,255,255)';
				ctx.fillText('ID: ' + player.ID, player.coord._x, player.coord._y);
				ctx.fillText('(' + player.coord._x.toFixed(2) + ',' + player.coord._y.toFixed(2) + ')', player.coord._x, player.coord._y + 20);
			}
		}
	};

	var env = function () {
		ctxEnv.save();
		ctxEnv.rotate(game.env.angle);
		ctxEnv.fillStyle = 'rgb(0, 0, 0)';
		ctxEnv.fillRect(-canvasWidth, -canvasHeight, canvasWidth + game.env.bias, 2 * canvasHeight) ;
		ctxEnv.restore();

		ctx.save();

		//Day & night
		ctx.rotate(game.env.angle);

		ctx.fillStyle = 'rgba(0,0,0, 0.2)';
		ctx.fillRect(-2000, -2000, 2000 + game.env.bias, 4000) ;

		//Moon
		// ctx.translate(-450, 0);
		// ctx.rotate(-game.env.time);
		
		// ctx.beginPath();
		// ctx.arc(0, 0, 50, 0, 2 * Math.PI);
		// ctx.fillStyle = "rgb(0,0,255)";
		// ctx.fill();
		// ctx.beginPath();
		// ctx.arc(25, 0 , 43, 0, 2 * Math.PI);
		// ctx.fillStyle = "rgb(229,229,229)";
		// ctx.fill();

		//Sun

		ctx.restore();
	};
	
	var UI = function () {	
		timeBox.innerHTML = gameDisplayTime;
		scoreBox.innerHTML = gameDisplayScore;
	};

	var debugWindow = function () {
		ctx.font = '12px Consolas';

		// game.env.bias
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('game.env.bias: ' + game.env.bias, -canvasWidthHalf + 21, -canvasHeightHalf + 21);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('game.env.bias: ' + game.env.bias, -canvasWidthHalf + 20, -canvasHeightHalf + 20);

		// game.env.bias
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('goal collision: ' + game.entities.goals[0].collision, -canvasWidthHalf + 21, -canvasHeightHalf + 41);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('goal collision: ' + game.entities.goals[0].collision, -canvasWidthHalf + 20, -canvasHeightHalf + 40);

		// timer
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('goal timer: ' + game.env.timer, -canvasWidthHalf + 21, -canvasHeightHalf + 61);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('goal timer: ' + game.env.timer, -canvasWidthHalf + 20, -canvasHeightHalf + 60);
	};

	return {
		canvas: canvas,
		planet: planet,
		env: env,
		goals: goals,
		players: players,
		UI: UI,
		debugWindow: debugWindow
	};

}());