/* 
 *	_environment.js
 */

 var game = game || {};

game.Environment = {
	baseBurnRate: 0.003,
	baseSpeed: Math.PI / (360 * 2),
	biasStep: 0,
	burnRate: 0,
	bias: 0,
	angle: 0,
	cycle: 0,
	timer: 0,

	create: function () {
		var obj = Object.create(this);
		obj.cycle = 15;
		obj.timer =  obj.cycle;
		obj.burnRate = obj.baseBurnRate;
		obj.speed = obj.baseSpeed;
		obj.biasStep = planetRadius * obj.burnRate;

		return obj;
	},

	//advance the environment by one frame
	tick: function() {
		this.angle += this.speed;
		this.timer -=  1 / fps ;
	},

	//check the state of the object to see whether it's in day (1) or night (-1)
	state: function (x, y) {
		return ctxEnv.getImageData(canvasWidthHalf + x, canvasHeightHalf + y, 1, 1).data[3] != 255 ;
	},

	shift: function (sign, factor) {
		this.bias = this.bias - sign * (planetRadius * this.burnRate) * factor;
	},

	consumed: function () {
		return Math.abs(this.bias) >= planetRadius;
	},

	oneUp: function() {
		gameDisplayScore += 1;

		this.timer += 2.5;
		this.speed += 0.10 * this.baseSpeed ;
		this.burnRate += 0.0001;
	},

	timeUp: function() {
		return this.timer <= 0;
	}
};