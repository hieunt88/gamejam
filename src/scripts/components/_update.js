/* 
 *	_update.js
 */

game.update = (function () {
	'use strict';

	var checkCollision = function (a, b) {
		var aHalfW = a.width * 0.5,
			aHalfH = a.height * 0.5,
			bHalfW = b.width * 0.5,
			bHalfH = b.height * 0.5;

		var aAngle = a.angle + (Math.PI * 0.5),
			bAngle = b.angle + (Math.PI * 0.5),
			aOffsetX = Math.cos(aAngle) * (a.position._y - aHalfH),
			aOffsetY = Math.sin(aAngle) * (a.position._y - aHalfH),
			bOffsetX = Math.cos(bAngle) * (b.position._y - bHalfH),
			bOffsetY = Math.sin(bAngle) * (b.position._y - bHalfH);

		a.collisionBox.p1.x = aOffsetX - aHalfW; 
		a.collisionBox.p1.y = aOffsetY - aHalfH; 
		a.collisionBox.p2.x = aOffsetX + aHalfW; 
		a.collisionBox.p2.y = aOffsetY + aHalfH; 
		
		b.collisionBox.p1.x = bOffsetX - bHalfW; 
		b.collisionBox.p1.y = bOffsetY - bHalfH; 
		b.collisionBox.p2.x = bOffsetX + bHalfW; 
		b.collisionBox.p2.y = bOffsetY + bHalfH; 

		return !(
					a.collisionBox.p2.x < b.collisionBox.p1.x || 
					b.collisionBox.p2.x < a.collisionBox.p1.x || 
					a.collisionBox.p2.y < b.collisionBox.p1.y || 
					b.collisionBox.p2.y < a.collisionBox.p1.y
				);
	};

	var canvas = function () {

	};

	var env = function () {
		game.env.tick();
		gameDisplayTime = game.env.timer.toFixed(1);
	};

	var goals = function () {
		var goal = game.entities.Goal.current();
		
		goal.position._y = -planetRadius - (goal.height + 10) - goal.offsetHeight;

		var coordAngle = goal.angle + (Math.PI * 0.5);
		goal.coord._x = Math.cos(coordAngle) * goal.position._y;
		goal.coord._y = Math.sin(coordAngle) * goal.position._y;

		goal.lit = game.env.state(goal.coord._x, goal.coord._y);


		// Check player collisions
		for (var ii = 0; ii < game.entities.players.length; ii++) {
			var player = game.entities.players[ii];

			goal.collision = checkCollision(goal, player);
			
			if (goal.collision === true && goal.sign === player.sign && (goal.lit ? 1 : -1) === goal.sign) {
				var angle = goal.angle + Math.PI + ((Math.random() - 0.5) * 2) * (Math.PI * 0.5);

				game.entities.Goal.create(0, 0, angle, Math.random() * 40);
				game.entities.Goal.current().position._y = -planetRadius - (goal.height + 10) - goal.offsetHeight;
				game.env.oneUp();
				break;
			}
		}
	};

	var players = function () {
		for (var i = 0; i < game.entities.players.length; i++) {
			var player = game.entities.players[i];

			player.lit = game.env.state(player.coord._x, player.coord._y);

			//burn if in the other section
			if ( (player.lit ? 1 : -1) * player.sign < 0) {
				game.env.shift(-player.sign, 1);
			}

			//speed up if in the right section
			if ((player.lit ? 1 : -1) * player.sign > 0) {
				player.speed = player.baseSpeed * 1.5;
				player.maxVelocity = player.baseMaxVelocity * 1.5;
			} else { //return to normal speed
				player.speed = player.baseSpeed;
				player.maxVelocity = player.baseMaxVelocity;
			}

			// Skill action1
			if (game.keymappings.keys[game.keymappings.playerKeys[i].action1] === true) {
				if (game.keymappings.holdKeys[game.keymappings.playerKeys[i].action1] !== true) {
					game.keymappings.holdKeys[game.keymappings.playerKeys[i].action1] = true;
					game.env.shift(player.sign, 2);
					// player.speedUp();
				}
			}

			// Skill action2
			if (game.keymappings.keys[game.keymappings.playerKeys[i].action2] === true) {
			}

			// LEFT (anti-clockwise)
			if (game.keymappings.keys[game.keymappings.playerKeys[i].left] === true) {
				if (player.velocity._x > -player.maxVelocity) {
					player.velocity._x -= player.speed;
				} else {
					player.velocity._x = -player.maxVelocity;
				}
			}
			// RIGHT (clockwise)
			if (game.keymappings.keys[game.keymappings.playerKeys[i].right] === true) {
				if (player.velocity._x < player.maxVelocity) {
					player.velocity._x += player.speed;
				} else {
					player.velocity._x = player.maxVelocity;
				}
			}
			// JUMP / UP
			if (game.keymappings.keys[game.keymappings.playerKeys[i].up] === true) {
				if (player.jumping === false) {
					player.jumping = true;
					player.position._y -= 1;
					player.position._y -= 1;
					player.velocity._y = 10;
				} else {
					player.velocity._y -= (player.velocity._y * 0.1);
				}
			}	

			// Slow down lateral movement and stop if no keys pressed
			if (game.keymappings.keys[game.keymappings.playerKeys[i].left] === false && 
				game.keymappings.keys[game.keymappings.playerKeys[i].right] === false) {
				if (player.velocity._x !== 0) {
					if (player.velocity._x < 0.02 && player.velocity._x > -0.02) {
						player.velocity._x = 0;
					} else {
						player.velocity._x -= (player.velocity._x * 0.1);
					}
				}
			}

			if (player.jumping === true) {
				if (player.position._y <= -planetRadius) {
					// Reduce vertical velocity
					player.position._y -= player.velocity._y;
					player.velocity._y -= 0.5;
				} else {
					// Landed
					player.velocity._y = 0;
					player.position._y = -planetRadius;
					player.jumping = false;
				}
			}

			player.angle += (player.velocity._x / 100);

			var coordAngle = player.angle + (Math.PI * 0.5);
			player.coord._x = Math.cos(coordAngle) * player.position._y;
			player.coord._y = Math.sin(coordAngle) * player.position._y;

			//check collision
			var goal = game.entities.Goal.current();
			player.collision = checkCollision(player, goal);
		}
	};

	var gameOverCheck = function () {
		gameover = game.env.consumed();
		gameover = gameover ? gameover : game.env.timeUp();
	};

	return {
		canvas: canvas,
		env: env,
		goals: goals,
		players: players,
		gameOverCheck: gameOverCheck,
	};
}());