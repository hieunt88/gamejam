/* 
 *	_keymappings.js
 */

game.keymappings = (function () {
	'use strict';

	var keys = {
		KeyA : false,
		KeyD : false,
		ArrowLeft : false,
		ArrowRight : false,
	};

	var holdKeys = {};

	//player one key mapping
	var playerKeys = [ 
		{
			left : 'ArrowLeft',
			right : 'ArrowRight',
			up : 'ArrowUp',
			down : 'ArrowDown',
			action1 : 'Numpad1',
			action2 : 'Numpad2'
		},
		{
			left : 'KeyA',
			right : 'KeyD',
			up : 'KeyW',
			down : 'KeyS',
			action1 : 'KeyJ',
			action2 : 'KeyK'
		}
	];


	var keyInput = function (e) {
		if (e.type === 'keydown') {
			keys[e.code] = true;
		}

		if (e.type === 'keyup') {
			keys[e.code] = false;
			holdKeys[e.code] = false;
		}
	};

	var init = function () {
		window.addEventListener('keydown', keyInput, false);
		window.addEventListener('keyup', keyInput, false);
	};

	return {
		init: init,
		keys: keys,
		playerKeys: playerKeys,
		holdKeys: holdKeys
	};
}());