/* 
 *	_entities.js
 */

game.entities = (function () {
	'use strict';
	var players = [],
		goals = [];

	var vector = {
		_x: 0,
		_y: 0,

		create: function (x, y) {
			var obj = Object.create(this);
			obj._x = x;
			obj._y = y;
			return obj;
		}
	};
	
	var entityID = 0;

	var Player = {
		
		create: function (x, y, color1, color2, speed, angle, sign, maxVelocity) {
			var obj = Object.create(this);
			obj.ID = entityID;
			obj.sign = sign;
			obj.position = vector.create(x, y);
			obj.coord = vector.create(x, y);
			obj.width = 30;
			obj.height = 30;
			obj.color1 = color1;
			obj.color2 = color2;
			obj.baseSpeed = speed;
			obj.speed = speed;
			obj.baseMaxVelocity = maxVelocity;
			obj.maxVelocity = maxVelocity;
			obj.angle = angle;
			obj.velocity = vector.create(0, 0);
			obj.lit = null;
			obj.jumping = false;
			obj.collision = false;
			obj.collisionBox = {
				p1: {
					x: 0,
					y: 0
				},
				p2: {
					x: 0,
					y: 0
				}
			};
			
			entityID ++;
			players.push(obj);
		},

		//speed up 
		speedUp: function() {
			this.speed *= 2;
		}
	};

	var Goal = {
		count: 0,

		create: function (x, y, angle, offsetHeight) {
			var obj = Object.create(this);
			obj.ID = entityID;
			obj.position = vector.create(x, y);
			obj.coord = vector.create(x, y);
			obj.width = 15;
			obj.height = 15;
			obj.angle = angle;

			var sign = (Math.random() - 0.5) >= 0 ? 1 : -1;
			game.entities.Goal.count += sign;
			
			if (Math.abs(game.entities.Goal.count) > 3) {
				game.entities.Goal.count = 0;
				sign = -sign;
			}

			obj.sign = sign;
			obj.color1 =  obj.sign > 0 ? 'rgb(' + colors.lighter + ')' : 'rgb(' + colors.darker + ')';
			obj.color2 =  obj.sign > 0 ? 'rgb(' + colors.darker + ')' : 'rgb(' + colors.light + ')';
			obj.offsetHeight = offsetHeight;
			obj.lit = null;
			obj.collision = false;
			obj.collisionBox = {
				p1: {
					x: 0,
					y: 0
				},
				p2: {
					x: 0,
					y: 0
				}
			};
			
			entityID ++;
			goals.push(obj);
		},

		current: function () {
			return goals[goals.length - 1];
		}
	};

	return {
		vector: vector,
		Player: Player,
		players: players,
		Goal: Goal,
		goals: goals
	};

}());