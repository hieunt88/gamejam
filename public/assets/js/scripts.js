'use strict';

// Global vars
var game = game || {},
	canvas = document.getElementById("canvas"),
	canvasEnv = document.getElementById("canvasEnv"),
	canvasWidth = canvas.width = canvasEnv.width = document.body.clientWidth,
	canvasHeight = canvas.height = canvasEnv.height = document.body.clientHeight,
	canvasWidthHalf = canvasWidth * 0.5,
	canvasHeightHalf = canvasHeight * 0.5,
	ctx = canvas.getContext("2d"),
	ctxEnv = canvasEnv.getContext("2d"),
	fps = 60,
	gameover = false,
	debug = false,
	debugLogs = [];

	ctx.imageSmoothingEnabled = true;	
	ctxEnv.imageSmoothingEnabled = false;

var planet = document.getElementById("planet"),
	planetRadius = planet.offsetWidth * 0.5;

	console.log(planetRadius);

var playerMaxVelocity = 2,
	playerSpeedDefault = 0.2;
	
var goalCount = 1;

var colors = {
	lighter: '252, 252, 250',
	light: '231, 227, 202',
	dark: '43, 42, 60',
	darker: '24, 18, 32'
};


var gameContainer = document.getElementById('game'), 
	gameDisplayTime = null,
	gameDisplayScore = 0,
	// interfaceUpdateFrequency = 1,
	// interfaceTimer = 0,
	timeBox = document.getElementById('time'),
	scoreBox = document.getElementById('score');



// var logoImage = new Image();
// logoImage.src = 'public/assets/img/ggj-logo.svg';

/*
 *  -- Component Imports --
 */

/* 
 *	_keymappings.js
 */

game.keymappings = (function () {
	'use strict';

	var keys = {
		KeyA : false,
		KeyD : false,
		ArrowLeft : false,
		ArrowRight : false,
	};

	var holdKeys = {};

	//player one key mapping
	var playerKeys = [ 
		{
			left : 'ArrowLeft',
			right : 'ArrowRight',
			up : 'ArrowUp',
			down : 'ArrowDown',
			action1 : 'Numpad1',
			action2 : 'Numpad2'
		},
		{
			left : 'KeyA',
			right : 'KeyD',
			up : 'KeyW',
			down : 'KeyS',
			action1 : 'KeyJ',
			action2 : 'KeyK'
		}
	];


	var keyInput = function (e) {
		if (e.type === 'keydown') {
			keys[e.code] = true;
		}

		if (e.type === 'keyup') {
			keys[e.code] = false;
			holdKeys[e.code] = false;
		}
	};

	var init = function () {
		window.addEventListener('keydown', keyInput, false);
		window.addEventListener('keyup', keyInput, false);
	};

	return {
		init: init,
		keys: keys,
		playerKeys: playerKeys,
		holdKeys: holdKeys
	};
}());
/* 
 *	_update.js
 */

game.update = (function () {
	'use strict';

	var checkCollision = function (a, b) {
		var aHalfW = a.width * 0.5,
			aHalfH = a.height * 0.5,
			bHalfW = b.width * 0.5,
			bHalfH = b.height * 0.5;

		var aAngle = a.angle + (Math.PI * 0.5),
			bAngle = b.angle + (Math.PI * 0.5),
			aOffsetX = Math.cos(aAngle) * (a.position._y - aHalfH),
			aOffsetY = Math.sin(aAngle) * (a.position._y - aHalfH),
			bOffsetX = Math.cos(bAngle) * (b.position._y - bHalfH),
			bOffsetY = Math.sin(bAngle) * (b.position._y - bHalfH);

		a.collisionBox.p1.x = aOffsetX - aHalfW; 
		a.collisionBox.p1.y = aOffsetY - aHalfH; 
		a.collisionBox.p2.x = aOffsetX + aHalfW; 
		a.collisionBox.p2.y = aOffsetY + aHalfH; 
		
		b.collisionBox.p1.x = bOffsetX - bHalfW; 
		b.collisionBox.p1.y = bOffsetY - bHalfH; 
		b.collisionBox.p2.x = bOffsetX + bHalfW; 
		b.collisionBox.p2.y = bOffsetY + bHalfH; 

		return !(
					a.collisionBox.p2.x < b.collisionBox.p1.x || 
					b.collisionBox.p2.x < a.collisionBox.p1.x || 
					a.collisionBox.p2.y < b.collisionBox.p1.y || 
					b.collisionBox.p2.y < a.collisionBox.p1.y
				);
	};

	var canvas = function () {

	};

	var env = function () {
		game.env.tick();
		gameDisplayTime = game.env.timer.toFixed(1);
	};

	var goals = function () {
		var goal = game.entities.Goal.current();
		
		goal.position._y = -planetRadius - (goal.height + 10) - goal.offsetHeight;

		var coordAngle = goal.angle + (Math.PI * 0.5);
		goal.coord._x = Math.cos(coordAngle) * goal.position._y;
		goal.coord._y = Math.sin(coordAngle) * goal.position._y;

		goal.lit = game.env.state(goal.coord._x, goal.coord._y);


		// Check player collisions
		for (var ii = 0; ii < game.entities.players.length; ii++) {
			var player = game.entities.players[ii];

			goal.collision = checkCollision(goal, player);
			
			if (goal.collision === true && goal.sign === player.sign && (goal.lit ? 1 : -1) === goal.sign) {
				var angle = goal.angle + Math.PI + ((Math.random() - 0.5) * 2) * (Math.PI * 0.5);

				game.entities.Goal.create(0, 0, angle, Math.random() * 40);
				game.entities.Goal.current().position._y = -planetRadius - (goal.height + 10) - goal.offsetHeight;
				game.env.oneUp();
				break;
			}
		}
	};

	var players = function () {
		for (var i = 0; i < game.entities.players.length; i++) {
			var player = game.entities.players[i];

			player.lit = game.env.state(player.coord._x, player.coord._y);

			//burn if in the other section
			if ( (player.lit ? 1 : -1) * player.sign < 0) {
				game.env.shift(-player.sign, 1);
			}

			//speed up if in the right section
			if ((player.lit ? 1 : -1) * player.sign > 0) {
				player.speed = player.baseSpeed * 1.5;
				player.maxVelocity = player.baseMaxVelocity * 1.5;
			} else { //return to normal speed
				player.speed = player.baseSpeed;
				player.maxVelocity = player.baseMaxVelocity;
			}

			// Skill action1
			if (game.keymappings.keys[game.keymappings.playerKeys[i].action1] === true) {
				if (game.keymappings.holdKeys[game.keymappings.playerKeys[i].action1] !== true) {
					game.keymappings.holdKeys[game.keymappings.playerKeys[i].action1] = true;
					game.env.shift(player.sign, 2);
					// player.speedUp();
				}
			}

			// Skill action2
			if (game.keymappings.keys[game.keymappings.playerKeys[i].action2] === true) {
			}

			// LEFT (anti-clockwise)
			if (game.keymappings.keys[game.keymappings.playerKeys[i].left] === true) {
				if (player.velocity._x > -player.maxVelocity) {
					player.velocity._x -= player.speed;
				} else {
					player.velocity._x = -player.maxVelocity;
				}
			}
			// RIGHT (clockwise)
			if (game.keymappings.keys[game.keymappings.playerKeys[i].right] === true) {
				if (player.velocity._x < player.maxVelocity) {
					player.velocity._x += player.speed;
				} else {
					player.velocity._x = player.maxVelocity;
				}
			}
			// JUMP / UP
			if (game.keymappings.keys[game.keymappings.playerKeys[i].up] === true) {
				if (player.jumping === false) {
					player.jumping = true;
					player.position._y -= 1;
					player.position._y -= 1;
					player.velocity._y = 10;
				} else {
					player.velocity._y -= (player.velocity._y * 0.1);
				}
			}	

			// Slow down lateral movement and stop if no keys pressed
			if (game.keymappings.keys[game.keymappings.playerKeys[i].left] === false && 
				game.keymappings.keys[game.keymappings.playerKeys[i].right] === false) {
				if (player.velocity._x !== 0) {
					if (player.velocity._x < 0.02 && player.velocity._x > -0.02) {
						player.velocity._x = 0;
					} else {
						player.velocity._x -= (player.velocity._x * 0.1);
					}
				}
			}

			if (player.jumping === true) {
				if (player.position._y <= -planetRadius) {
					// Reduce vertical velocity
					player.position._y -= player.velocity._y;
					player.velocity._y -= 0.5;
				} else {
					// Landed
					player.velocity._y = 0;
					player.position._y = -planetRadius;
					player.jumping = false;
				}
			}

			player.angle += (player.velocity._x / 100);

			var coordAngle = player.angle + (Math.PI * 0.5);
			player.coord._x = Math.cos(coordAngle) * player.position._y;
			player.coord._y = Math.sin(coordAngle) * player.position._y;

			//check collision
			var goal = game.entities.Goal.current();
			player.collision = checkCollision(player, goal);
		}
	};

	var gameOverCheck = function () {
		gameover = game.env.consumed();
		gameover = gameover ? gameover : game.env.timeUp();
	};

	return {
		canvas: canvas,
		env: env,
		goals: goals,
		players: players,
		gameOverCheck: gameOverCheck,
	};
}());
/* 
 *	_render.js
 */

game.render = (function () {
	'use strict';

	var canvas = function () {
		ctx.clearRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);
		ctxEnv.clearRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);

		ctx.fillStyle = 'rgb(' + colors.dark + ')';
		ctx.strokeStyle = 'rgb(' + colors.lighter + ')';

		if (debug === true) {
			// ctx.fillStyle = 'rgb(255,255,255)';
		}

		ctx.fillRect(-canvasWidthHalf, -canvasHeightHalf, canvasWidth, canvasHeight);
	};

	// var imgTextureLight = new Image();
	// imgTextureLight.src = 'public/assets/img/tex-planet-light-1.jpg';

	var planet = function () {
		ctx.save();
		ctx.rotate(game.env.time + (Math.PI * 0.5));			
		// ctx.strokeStyle = "rgb(0,0,0)";
		ctx.beginPath();
		ctx.arc(0, 0, planetRadius, 0, Math.PI * 2, false);
		ctx.strokeStyle = colors.darker;
		ctx.stroke();
		// ctx.clip();

		// ctx.drawImage(logoImage, -500, -1000 - game.env.bias);
		
		ctx.restore();
		
	};

	var goals = function () {
		var goal = game.entities.Goal.current();

		ctx.save();

		ctx.fillStyle = goal.color1;
		ctx.strokeStyle = goal.color2;
		ctx.rotate(goal.angle);
		ctx.beginPath();
		ctx.arc(0, goal.position._y - (goal.height * 0.5), goal.width * 0.5, 0, Math.PI * 2, false);
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		if (debug) {
			// Collision box
			var halfW = goal.width * 0.5,
				halfH = goal.height * 0.5;

			ctx.strokeStyle = ctx.fillStyle = 'rgb(165,0,0)';
			ctx.beginPath();
			ctx.moveTo(goal.collisionBox.p1.x, goal.collisionBox.p1.y);
			ctx.lineTo(goal.collisionBox.p2.x, goal.collisionBox.p1.y);
			ctx.lineTo(goal.collisionBox.p2.x, goal.collisionBox.p2.y);
			ctx.lineTo(goal.collisionBox.p1.x, goal.collisionBox.p2.y);
			ctx.lineTo(goal.collisionBox.p1.x, goal.collisionBox.p1.y);
			ctx.stroke();

			if (goal.collision) {
				ctx.fill();
			}

			// Goal coords
			ctx.fillStyle = 'rgb(0,0,0)';
			ctx.fillText('ID: ' + goal.ID, goal.coord._x + 1, goal.coord._y + 1);
			ctx.fillText('(' + goal.coord._x.toFixed(2) + ',' + goal.coord._y.toFixed(2) + ')', goal.coord._x + 1, goal.coord._y + 21);
			ctx.fillStyle = 'rgb(255,255,255)';
			ctx.fillText('ID: ' + goal.ID, goal.coord._x, goal.coord._y);
			ctx.fillText('(' + goal.coord._x.toFixed(2) + ',' + goal.coord._y.toFixed(2) + ')', goal.coord._x, goal.coord._y + 20);
		}
	};

	var players = function () {
		for (var i = 0; i < game.entities.players.length; i++) {
			var player = game.entities.players[i];

			// if (player.lit === true) {
			// 	ctx.fillStyle = 'rgb(' + colors.light + ')';
			// 	ctx.strokeStyle = 'rgb(' + colors.dark + ')';
			// } else {
			// 	ctx.fillStyle = 'rgb(' + colors.dark + ')';
			// 	ctx.strokeStyle = 'rgb(' + colors.lighter + ')';
			// }

			ctx.fillStyle = player.color1;
			ctx.strokeStyle = player.color2;

			ctx.save();
			ctx.rotate(player.angle);

			ctx.lineWidth = 2;
			ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
			ctx.beginPath();
			ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.5, 0, Math.PI * 2, false);
			ctx.stroke();
			ctx.fill();
			
			if (player.ID === 0 && player.lit === true) {
				ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
				ctx.beginPath();
				ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.2, 0, Math.PI * 2, false);
				ctx.fillStyle = player.color2;
				ctx.fill();
			}

			if (player.ID === 1 && player.lit === false) {
				ctx.moveTo(player.position._x, player.position._y - (player.width * 0.5));
				ctx.beginPath();
				ctx.fillStyle = player.color2;
				ctx.arc(player.position._x, player.position._y - (player.width * 0.5), player.width * 0.2, 0, Math.PI * 2, false);
				ctx.fill();
			}

			// ctx.clip();
			// ctx.drawImage(player.texture, player.position._x - (player.width * 0.5), player.position._y - player.height);
			// ctx.stroke();

			// ctx.fillRect(player.position._x - (player.width * 0.5), player.position._y - player.height, player.width, player.height);
			ctx.restore();

			if (debug) {
				// Collision box
				var halfW = player.width * 0.5,
					halfH = player.height * 0.5;

				ctx.strokeStyle = ctx.fillStyle = 'rgb(165,0,0)';
				ctx.beginPath();
				ctx.moveTo(player.collisionBox.p1.x, player.collisionBox.p1.y);
				ctx.lineTo(player.collisionBox.p2.x, player.collisionBox.p1.y);
				ctx.lineTo(player.collisionBox.p2.x, player.collisionBox.p2.y);
				ctx.lineTo(player.collisionBox.p1.x, player.collisionBox.p2.y);
				ctx.lineTo(player.collisionBox.p1.x, player.collisionBox.p1.y);
				ctx.stroke();

				if (player.collision) {
					ctx.fill();
				}

				// Player coords
				ctx.fillStyle = 'rgb(0,0,0)';
				ctx.fillText('ID: ' + player.ID, player.coord._x + 1, player.coord._y + 1);
				ctx.fillText('(' + player.coord._x.toFixed(2) + ',' + player.coord._y.toFixed(2) + ')', player.coord._x + 1, player.coord._y + 21);
				ctx.fillStyle = 'rgb(255,255,255)';
				ctx.fillText('ID: ' + player.ID, player.coord._x, player.coord._y);
				ctx.fillText('(' + player.coord._x.toFixed(2) + ',' + player.coord._y.toFixed(2) + ')', player.coord._x, player.coord._y + 20);
			}
		}
	};

	var env = function () {
		ctxEnv.save();
		ctxEnv.rotate(game.env.angle);
		ctxEnv.fillStyle = 'rgb(0, 0, 0)';
		ctxEnv.fillRect(-canvasWidth, -canvasHeight, canvasWidth + game.env.bias, 2 * canvasHeight) ;
		ctxEnv.restore();

		ctx.save();

		//Day & night
		ctx.rotate(game.env.angle);

		ctx.fillStyle = 'rgba(0,0,0, 0.2)';
		ctx.fillRect(-2000, -2000, 2000 + game.env.bias, 4000) ;

		//Moon
		// ctx.translate(-450, 0);
		// ctx.rotate(-game.env.time);
		
		// ctx.beginPath();
		// ctx.arc(0, 0, 50, 0, 2 * Math.PI);
		// ctx.fillStyle = "rgb(0,0,255)";
		// ctx.fill();
		// ctx.beginPath();
		// ctx.arc(25, 0 , 43, 0, 2 * Math.PI);
		// ctx.fillStyle = "rgb(229,229,229)";
		// ctx.fill();

		//Sun

		ctx.restore();
	};
	
	var UI = function () {	
		timeBox.innerHTML = gameDisplayTime;
		scoreBox.innerHTML = gameDisplayScore;
	};

	var debugWindow = function () {
		ctx.font = '12px Consolas';

		// game.env.bias
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('game.env.bias: ' + game.env.bias, -canvasWidthHalf + 21, -canvasHeightHalf + 21);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('game.env.bias: ' + game.env.bias, -canvasWidthHalf + 20, -canvasHeightHalf + 20);

		// game.env.bias
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('goal collision: ' + game.entities.goals[0].collision, -canvasWidthHalf + 21, -canvasHeightHalf + 41);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('goal collision: ' + game.entities.goals[0].collision, -canvasWidthHalf + 20, -canvasHeightHalf + 40);

		// timer
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillText('goal timer: ' + game.env.timer, -canvasWidthHalf + 21, -canvasHeightHalf + 61);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.fillText('goal timer: ' + game.env.timer, -canvasWidthHalf + 20, -canvasHeightHalf + 60);
	};

	return {
		canvas: canvas,
		planet: planet,
		env: env,
		goals: goals,
		players: players,
		UI: UI,
		debugWindow: debugWindow
	};

}());
/* 
 *	_event-loop.js
 */

game.eventLoop = (function () {
	'use strict';

	var paused = false,
		pauseStep = false,
		dt = 0,
		dtInit = Date.now(); //new Date();

	var togglePaused = function (e) {
		paused = !paused;

		if (paused) {
		 	gameContainer.classList.add('game-paused');
		} else {
			gameContainer.classList.remove('game-paused');
		}
	};

	var calculateDeltaTime = function () {
		var dtNow = new Date();
		dt = dtNow - dtInit;
		// fps = 1 / dt;
	};

	var loop = function () {
		setTimeout(function () {

			if (game.keymappings.keys.ControlLeft === true) {
				pauseStep = true;
			}

			if (gameover === false) {
				if (paused === false || pauseStep === true) {
					pauseStep = false;

					game.update.canvas();
					game.update.env();
					game.update.players();
					game.update.goals();
					game.update.gameOverCheck();

					game.render.canvas();
					game.render.env();
					// game.render.planet();
					game.render.players();
					game.render.goals();
					game.render.UI();

					if (debug) {
						game.render.debugWindow();
					}

					// var gamePads = navigator.getGamepads();
					// console.log(gamePads[1]);
				}
			}

			window.requestAnimationFrame(loop);
			
		}, 1000 / fps);
	};

	var init = function () {		
				
		document.addEventListener('click', togglePaused, false);
		ctx.translate(canvasWidth * 0.5, canvasHeight * 0.5);
		ctxEnv.translate(canvasWidth * 0.5, canvasHeight * 0.5);

		game.env = game.Environment.create();
		timeBox.innerHTML = gameDisplayTime;

		// Create players
		game.entities.Player.create(
			0,
			-planetRadius,
			'rgb(' + colors.lighter + ')',
			'rgb(' + colors.darker + ')',
			playerSpeedDefault,
			Math.PI * 0.5,
			1,
			playerMaxVelocity
		);

		game.entities.Player.create(
			0,
			-planetRadius,
			'rgb(' + colors.darker + ')',
			'rgb(' + colors.lighter + ')',
			playerSpeedDefault,
			Math.PI * 1.5,
			-1, 
			playerMaxVelocity
		);

		game.entities.Goal.create(0, 0, Math.random() * (Math.PI * 2), 0);

		// if (paused === false) {
			loop();
		// }
	};

	return {
		dt: dt,
		init: init
	};

}());
/* 
 *	_entities.js
 */

game.entities = (function () {
	'use strict';
	var players = [],
		goals = [];

	var vector = {
		_x: 0,
		_y: 0,

		create: function (x, y) {
			var obj = Object.create(this);
			obj._x = x;
			obj._y = y;
			return obj;
		}
	};
	
	var entityID = 0;

	var Player = {
		
		create: function (x, y, color1, color2, speed, angle, sign, maxVelocity) {
			var obj = Object.create(this);
			obj.ID = entityID;
			obj.sign = sign;
			obj.position = vector.create(x, y);
			obj.coord = vector.create(x, y);
			obj.width = 30;
			obj.height = 30;
			obj.color1 = color1;
			obj.color2 = color2;
			obj.baseSpeed = speed;
			obj.speed = speed;
			obj.baseMaxVelocity = maxVelocity;
			obj.maxVelocity = maxVelocity;
			obj.angle = angle;
			obj.velocity = vector.create(0, 0);
			obj.lit = null;
			obj.jumping = false;
			obj.collision = false;
			obj.collisionBox = {
				p1: {
					x: 0,
					y: 0
				},
				p2: {
					x: 0,
					y: 0
				}
			};
			
			entityID ++;
			players.push(obj);
		},

		//speed up 
		speedUp: function() {
			this.speed *= 2;
		}
	};

	var Goal = {
		count: 0,

		create: function (x, y, angle, offsetHeight) {
			var obj = Object.create(this);
			obj.ID = entityID;
			obj.position = vector.create(x, y);
			obj.coord = vector.create(x, y);
			obj.width = 15;
			obj.height = 15;
			obj.angle = angle;

			var sign = (Math.random() - 0.5) >= 0 ? 1 : -1;
			game.entities.Goal.count += sign;
			
			if (Math.abs(game.entities.Goal.count) > 3) {
				game.entities.Goal.count = 0;
				sign = -sign;
			}

			obj.sign = sign;
			obj.color1 =  obj.sign > 0 ? 'rgb(' + colors.lighter + ')' : 'rgb(' + colors.darker + ')';
			obj.color2 =  obj.sign > 0 ? 'rgb(' + colors.darker + ')' : 'rgb(' + colors.light + ')';
			obj.offsetHeight = offsetHeight;
			obj.lit = null;
			obj.collision = false;
			obj.collisionBox = {
				p1: {
					x: 0,
					y: 0
				},
				p2: {
					x: 0,
					y: 0
				}
			};
			
			entityID ++;
			goals.push(obj);
		},

		current: function () {
			return goals[goals.length - 1];
		}
	};

	return {
		vector: vector,
		Player: Player,
		players: players,
		Goal: Goal,
		goals: goals
	};

}());
/* 
 *	_environment.js
 */

 var game = game || {};

game.Environment = {
	baseBurnRate: 0.003,
	baseSpeed: Math.PI / (360 * 2),
	biasStep: 0,
	burnRate: 0,
	bias: 0,
	angle: 0,
	cycle: 0,
	timer: 0,

	create: function () {
		var obj = Object.create(this);
		obj.cycle = 15;
		obj.timer =  obj.cycle;
		obj.burnRate = obj.baseBurnRate;
		obj.speed = obj.baseSpeed;
		obj.biasStep = planetRadius * obj.burnRate;

		return obj;
	},

	//advance the environment by one frame
	tick: function() {
		this.angle += this.speed;
		this.timer -=  1 / fps ;
	},

	//check the state of the object to see whether it's in day (1) or night (-1)
	state: function (x, y) {
		return ctxEnv.getImageData(canvasWidthHalf + x, canvasHeightHalf + y, 1, 1).data[3] != 255 ;
	},

	shift: function (sign, factor) {
		this.bias = this.bias - sign * (planetRadius * this.burnRate) * factor;
	},

	consumed: function () {
		return Math.abs(this.bias) >= planetRadius;
	},

	oneUp: function() {
		gameDisplayScore += 1;

		this.timer += 2.5;
		this.speed += 0.10 * this.baseSpeed ;
		this.burnRate += 0.0001;
	},

	timeUp: function() {
		return this.timer <= 0;
	}
};

/*
 *  -- Component Functions --
 */

document.addEventListener("DOMContentLoaded", function() {
	game.keymappings.init();
	game.eventLoop.init();
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJnYW1lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIEdsb2JhbCB2YXJzXHJcbnZhciBnYW1lID0gZ2FtZSB8fCB7fSxcclxuXHRjYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNhbnZhc1wiKSxcclxuXHRjYW52YXNFbnYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNhbnZhc0VudlwiKSxcclxuXHRjYW52YXNXaWR0aCA9IGNhbnZhcy53aWR0aCA9IGNhbnZhc0Vudi53aWR0aCA9IGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGgsXHJcblx0Y2FudmFzSGVpZ2h0ID0gY2FudmFzLmhlaWdodCA9IGNhbnZhc0Vudi5oZWlnaHQgPSBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodCxcclxuXHRjYW52YXNXaWR0aEhhbGYgPSBjYW52YXNXaWR0aCAqIDAuNSxcclxuXHRjYW52YXNIZWlnaHRIYWxmID0gY2FudmFzSGVpZ2h0ICogMC41LFxyXG5cdGN0eCA9IGNhbnZhcy5nZXRDb250ZXh0KFwiMmRcIiksXHJcblx0Y3R4RW52ID0gY2FudmFzRW52LmdldENvbnRleHQoXCIyZFwiKSxcclxuXHRmcHMgPSA2MCxcclxuXHRnYW1lb3ZlciA9IGZhbHNlLFxyXG5cdGRlYnVnID0gZmFsc2UsXHJcblx0ZGVidWdMb2dzID0gW107XHJcblxyXG5cdGN0eC5pbWFnZVNtb290aGluZ0VuYWJsZWQgPSB0cnVlO1x0XHJcblx0Y3R4RW52LmltYWdlU21vb3RoaW5nRW5hYmxlZCA9IGZhbHNlO1xyXG5cclxudmFyIHBsYW5ldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGxhbmV0XCIpLFxyXG5cdHBsYW5ldFJhZGl1cyA9IHBsYW5ldC5vZmZzZXRXaWR0aCAqIDAuNTtcclxuXHJcblx0Y29uc29sZS5sb2cocGxhbmV0UmFkaXVzKTtcclxuXHJcbnZhciBwbGF5ZXJNYXhWZWxvY2l0eSA9IDIsXHJcblx0cGxheWVyU3BlZWREZWZhdWx0ID0gMC4yO1xyXG5cdFxyXG52YXIgZ29hbENvdW50ID0gMTtcclxuXHJcbnZhciBjb2xvcnMgPSB7XHJcblx0bGlnaHRlcjogJzI1MiwgMjUyLCAyNTAnLFxyXG5cdGxpZ2h0OiAnMjMxLCAyMjcsIDIwMicsXHJcblx0ZGFyazogJzQzLCA0MiwgNjAnLFxyXG5cdGRhcmtlcjogJzI0LCAxOCwgMzInXHJcbn07XHJcblxyXG5cclxudmFyIGdhbWVDb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ2FtZScpLCBcclxuXHRnYW1lRGlzcGxheVRpbWUgPSBudWxsLFxyXG5cdGdhbWVEaXNwbGF5U2NvcmUgPSAwLFxyXG5cdC8vIGludGVyZmFjZVVwZGF0ZUZyZXF1ZW5jeSA9IDEsXHJcblx0Ly8gaW50ZXJmYWNlVGltZXIgPSAwLFxyXG5cdHRpbWVCb3ggPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndGltZScpLFxyXG5cdHNjb3JlQm94ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Njb3JlJyk7XHJcblxyXG5cclxuXHJcbi8vIHZhciBsb2dvSW1hZ2UgPSBuZXcgSW1hZ2UoKTtcclxuLy8gbG9nb0ltYWdlLnNyYyA9ICdwdWJsaWMvYXNzZXRzL2ltZy9nZ2otbG9nby5zdmcnO1xyXG5cclxuLypcclxuICogIC0tIENvbXBvbmVudCBJbXBvcnRzIC0tXHJcbiAqL1xyXG5cclxuLy8gaW1wb3J0KCdjb21wb25lbnRzL19rZXltYXBwaW5ncy5qcycpO1xyXG4vLyBpbXBvcnQoJ2NvbXBvbmVudHMvX3VwZGF0ZS5qcycpO1xyXG4vLyBpbXBvcnQoJ2NvbXBvbmVudHMvX3JlbmRlci5qcycpO1xyXG4vLyBpbXBvcnQoJ2NvbXBvbmVudHMvX2V2ZW50LWxvb3AuanMnKTtcclxuLy8gaW1wb3J0KCdjb21wb25lbnRzL19lbnRpdGllcy5qcycpO1xyXG4vLyBpbXBvcnQoJ2NvbXBvbmVudHMvX2Vudmlyb25tZW50LmpzJyk7XHJcblxyXG4vKlxyXG4gKiAgLS0gQ29tcG9uZW50IEZ1bmN0aW9ucyAtLVxyXG4gKi9cclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGZ1bmN0aW9uKCkge1xyXG5cdGdhbWUua2V5bWFwcGluZ3MuaW5pdCgpO1xyXG5cdGdhbWUuZXZlbnRMb29wLmluaXQoKTtcclxufSk7XHJcbiJdLCJmaWxlIjoiZ2FtZS5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
